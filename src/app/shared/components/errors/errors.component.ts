import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'buy-error',
  templateUrl: './errors.component.html',
  styleUrls: ['./errors.component.scss']
})

export class ErrorsComponent implements OnInit {
  @Input() public control;
  public ngOnInit() {}
}
