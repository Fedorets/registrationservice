import { Component, Input } from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';

@Component({
  selector: 'z-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss']
})

export class DatepickerComponent {
  @Input() public label: string;
  @Input() public placeholder: string;
  @Input() public myDatePickerOptions: IMyDpOptions;
  @Input() public control;
  public onDateChanged(e) {
    this.control.setValue(e.jsdate);
  }
}
