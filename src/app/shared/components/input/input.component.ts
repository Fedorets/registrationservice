
import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'z-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})

export class InputComponent {
  @Input() public type = 'text';
  @Input() public label = null;
  @Input() public placeholder = '';
  @Input() public id = 'IdOfElement';
  @Input() public isTextarea = false;
  @Input() public control: FormControl = null;
  constructor() {
   // console.log(this.control);
  }
}
