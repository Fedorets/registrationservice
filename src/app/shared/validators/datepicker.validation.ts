import { AbstractControl } from '@angular/forms';
import * as moment from 'moment';

export class DatepickerValidation {
  public static checkAge() {
    return (c: AbstractControl) => {
       if (c.value !== null) {
         let currentDate = new Date();
         let year = moment(c.value).format('YYYY');
         let result = currentDate.getFullYear() - parseInt(year, 10) < 18;
         return  result ? { agelimit: true } : null;
      } else { return null; }
    };
  }
}
