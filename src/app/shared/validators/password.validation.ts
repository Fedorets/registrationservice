import { AbstractControl } from '@angular/forms';

export class PasswordValidation {
  public static matchPassword(c: AbstractControl) {
    let password = c.get('password').value;
    let confirmPassword = c.get('confirmPassword').value;
    if (password !== confirmPassword) {
      c.get('confirmPassword').setErrors( { matchPassword: true } );
    } else { return null; }
  }
}
