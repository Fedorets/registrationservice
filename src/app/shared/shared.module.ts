import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { RouterModule } from '@angular/router';
import { SelectModule } from 'ng-select';

import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { InputComponent } from './components/input/input.component';
import { ErrorsComponent } from './components/errors/errors.component';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { DatepickerComponent } from './components/datepicker/datepicker.component';
import { SelectComponent } from './components/select/select.component';
import {
  WrapperEditInputComponent
} from './components/wrapperEditInput/wrapperEditInput.component';
import { WrapperEditSelectComponent } from './components/wrapperEditSelect/wrapperEditSelect.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    MyDatePickerModule,
    ReactiveFormsModule,
    SelectModule
  ],
  declarations: [
    FooterComponent,
    HeaderComponent,
    InputComponent,
    ErrorsComponent,
    CheckboxComponent,
    DatepickerComponent,
    SelectComponent,
    WrapperEditInputComponent,
    WrapperEditSelectComponent
  ],
  exports: [
    FooterComponent,
    HeaderComponent,
    CheckboxComponent,
    InputComponent,
    ErrorsComponent,
    DatepickerComponent,
    WrapperEditInputComponent,
    WrapperEditSelectComponent,
    SelectComponent
  ],
  providers: []
})

export class SharedModule {}
