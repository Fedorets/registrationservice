export class RegExpService {
  public static email = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;
  public static password = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/;
}
