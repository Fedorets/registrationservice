import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import {  ActivatedRouteSnapshot,
          CanActivate,
          Router,
          RouterStateSnapshot
        } from '@angular/router';

import { AuthService } from '../core/services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router,
              public authService: AuthService) {
  }

  /**
   * Used for protection pages
   * @param next - current route
   * @returns permission
   */
  public canActivate(next: ActivatedRouteSnapshot,
                     state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let status = next.data.status;
    if (status === 'Admin' && this.authService.isLogin()) {
      return true;
    } else if (status === 'Visitor' && !this.authService.isLogin()) {
      return true;
    } else if (status === 'Admin' && !this.authService.isLogin()) {
      this.router.navigate(['login/signIn']); return false;
    } else { this.router.navigate(['']); return false; }
  }
}
