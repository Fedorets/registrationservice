import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { UserData } from '../core/models/userData.model';
import { ProfileService } from '../core/services/profile.service';
import { RegExpService } from '../shared/services/regExp.service';

@Component({
  selector: 'buy-home',
  styleUrls: [ './home.component.scss' ],
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  public userData: UserData;
  public isEdit: boolean;
  public detailForm: FormGroup;
  public incomeData;
  constructor(private profileService: ProfileService) {
    this.isEdit = false;
    this.profileService.getCurrentUser().then((res: UserData) => {
       this.userData = res;
    });
    this.incomeData = [
      {
        value: '1000',
        label: '1000$'
      },
      {
        value: '2000',
        label: '2000$'
      },
      {
        value: '3000',
        label: '3000$'
      },
      {
        value: '4000',
        label: '4000$'
      },
      {
        value: '5000',
        label: '5000$'
      }
    ];
  }
  public edit(form) {
    this.isEdit = true;
  }
  public save(form) {
    if (!form.invalid) {
      this.isEdit = false;
      this.profileService.editCurrentUser(form.value);
    }
  }
  public ngOnInit() {
    this.detailForm = new FormGroup({
      name: new FormControl(this.userData.name),
      surname: new FormControl(this.userData.surname),
      dateOfBirth: new FormControl(this.userData.date()),
      phone: new FormControl(this.userData.phone),
      address: new FormControl(this.userData.address),
      city: new FormControl(this.userData.city),
      country: new FormControl(this.userData.country),
      username: new FormControl(this.userData.username, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(15)
      ]),
      password: new FormControl(this.userData.password, [
        Validators.required,
        Validators.minLength(6),
        Validators.pattern(RegExpService.password)
      ]),
      placeOfEmployment: new FormControl(this.userData.placeOfEmployment),
      annualIncome: new FormControl(this.userData.annualIncome),
      sport: new FormControl(this.userData.sport)
    });
  }
}
