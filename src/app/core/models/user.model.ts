export class User {
  public username: string;
  public password: string;
  constructor(data) {
    this.username = data.username;
    this.password = data.password;
  }
}
