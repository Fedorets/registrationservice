import * as moment from 'moment';

export class UserData {
  public name: string;
  public surname: string;
  public username: string;
  public password: string;
  public confirmPassword: string;
  public dateOfBirth: string;
  public phone: string;
  public address: string;
  public city: string;
  public country: string;
  public annualIncome: string;
  public emplStatus: string;
  public permission: boolean;
  public placeOfEmployment: string;
  public sport: string;

  constructor(data) {
    this.name = data.name;
    this.surname = data.surname;
    this.username = data.username;
    this.password = data.password;
    this.confirmPassword = data.confirmPassword;
    this.dateOfBirth = data.dateOfBirth;
    this.phone = data.phone;
    this.address = data.address;
    this.city = data.city;
    this.country = data.country;
    this.annualIncome = data.annualIncome;
    this.emplStatus = data.emplStatus;
    this.permission = data.permission;
    this.placeOfEmployment = data.placeOfEmployment;
    this.sport = data.sport;
  }

  public date() {
    return moment(this.dateOfBirth).format('L');
  }
}
