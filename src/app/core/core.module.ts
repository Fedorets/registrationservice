import { NgModule } from '@angular/core';

import { AuthService } from './services/auth.service';
import { LocalStorageService } from './services/localStorage.service';
import { ProfileService } from './services/profile.service';

@NgModule({
  providers: [
    AuthService,
    ProfileService,
    LocalStorageService
  ]
})

export class CoreModule {}
