import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable()
export class LocalStorageService {
  public users;

  constructor() {
    this.users = this.getAllUsers();
  }

  public signUp(data) {
    return new Promise((resolve) => {
      if (this.isUserExist(data.username)) {
        resolve('Error. Username exist.');
      } else {
        this.users.push(data);
        this.setUsers(this.users);
        resolve(true);
      }
    });
  }

  public editCurrentUser(data) {
    return new Promise((resolve) => {
      this.users = [];
      let users = this.getAllUsers();
      users.forEach((i) => {
        if (i.username === data.username) {
          i = data;
        }
        this.users.push(i);
      });
      this.setUsers(this.users);
      resolve(true);
    });
  }

  public signIn(data) {
    return new Promise((resolve) => {
      if (this.isUserAuthenticated(data)) {
        localStorage.setItem('user', JSON.stringify(data));
        resolve(data);
      } else { resolve({ error: 'Password or username is incorrect'}); }
    });
  }

  public logout() {
    return new Promise((resolve) => {
      localStorage.removeItem('user');
      resolve(true);
    });
  }

  public isLogin() {
    let tmp = localStorage.getItem('user');
    return (tmp !== null);
  }

  public findCurrentUser() {
    return new Promise((resolve) => {
      let currentUser = JSON.parse(localStorage.getItem('user'));
      let users = this.getAllUsers();
      let user = users.find((i) => {
        return i.username === currentUser.username;
      });
      resolve(user);
    });
  }

  public getAllUsers() {
    let tmp = JSON.parse(localStorage.getItem('users'));
    tmp = (tmp !== null) ? tmp : [];
    return tmp;
  }

  public setUsers(data) {
    localStorage.setItem('users', JSON.stringify(data));
    return true;
  }

  public isUserExist(username) {
    let users = this.getAllUsers();
    let user = users.find((i) => {
      return i.username === username;
    });
    return user !== undefined;
  }

  public isUserAuthenticated(data) {
    console.log('Check isUserAuthenticated');
    let users = this.getAllUsers();
    console.log(users);
    let user = users.find((i) => {
      return (i.username === data.username && i.password === data.password);
    });
    return user !== undefined;
  }
}
