import { Injectable } from '@angular/core';

import { LocalStorageService } from './localStorage.service';
import { User } from '../models/user.model';

@Injectable()
export class AuthService {
  constructor(private localStorageService: LocalStorageService) {}
  public signUp(data) {
    return new Promise((resolve) => {
      this.localStorageService.signUp(data).then((res) => {
        resolve(res);
      });
    });
  }

  public signIn(data) {
    return new Promise((resolve) => {
      this.localStorageService.signIn(data).then((res: any) => {
        if (!res.error) {
          resolve(new User(res));
        } else { resolve(res); }
      });
    });
  }

  public logout() {
    return new Promise((resolve) => {
      this.localStorageService.logout().then((res) => {
        resolve(res);
      });
    });
  }

  public isLogin() {
    return this.localStorageService.isLogin();
  }

}
