import { Injectable } from '@angular/core';

import { LocalStorageService } from './localStorage.service';
import { UserData } from '../models/userData.model';

@Injectable()
export class ProfileService {
  constructor(private localStorageService: LocalStorageService) {}

  public getCurrentUser() {
    return new Promise((resolve) => {
      this.localStorageService.findCurrentUser().then((res) => {
        resolve(new UserData(res));
      });
    });
  }

  public editCurrentUser(data) {
    return new Promise((resolve) => {
      this.localStorageService.editCurrentUser(data).then((res) => {
        resolve(res);
      });
    });
  }
}
