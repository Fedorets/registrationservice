import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Select2Module } from 'ng2-select2';

import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { routes } from './auth.routes';
import { SharedModule } from '../shared/shared.module';
import { Step1Component } from './sign-up/step1/step1.component';
import { Step2Component } from './sign-up/step2/step2.component';

@NgModule({
  declarations: [
    SignInComponent,
    SignUpComponent,
    Step1Component,
    Step2Component
  ],
  exports: [],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})

export class AuthModule {}
