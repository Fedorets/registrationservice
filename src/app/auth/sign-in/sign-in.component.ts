import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';
import { User } from '../../core/models/user.model';

@Component({
  selector: 'z-sign-in',
  styleUrls: [ './sign-in.component.scss' ],
  templateUrl: './sign-in.component.html'
})
export class SignInComponent {
  public signInForm: FormGroup;
  public serverErrors;

  constructor(private router: Router,
              private auth: AuthService) {
    this.signInForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }
  public submit($event, form) {
    $event.preventDefault();
    this.auth.signIn(form.value).then((res: any) => {
      if (res instanceof User) {
        console.log(res, res instanceof User);
        this.router.navigate(['../../']);
      } else { this.serverErrors = res.error; }
    });
  }
}
