import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { RegExpService } from '../../../shared/services/regExp.service';
import { PasswordValidation } from '../../../shared/validators/password.validation';

@Component({
  selector: 'z-step2',
  styleUrls: [ './step2.component.scss' ],
  templateUrl: './step2.component.html',
})
export class Step2Component {
  @Output() public step2FormDataEmit;
  public step2AuthForm: FormGroup;
  public statusData: any;
  public incomeData: any;
  public isEmployed: boolean;
  constructor(fb: FormBuilder) {
    this.step2FormDataEmit = new EventEmitter();
    this.step2AuthForm = fb.group({
      username: [null, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(15)
      ]],
      password: [null, [
        Validators.required,
        Validators.minLength(6),
        Validators.pattern(RegExpService.password)
      ]],
      confirmPassword: [null, Validators.required],
      emplStatus: [null, [
        Validators.required
      ]],
      annualIncome: [null],
      sport: [null],
      permission: ['', Validators.requiredTrue]
    }, {
      validator: PasswordValidation.matchPassword
    });
    this.step2AuthForm.get('emplStatus').valueChanges.subscribe((res) => {
      if (res === 'employed') {
        this.step2AuthForm.addControl('placeOfEmployment', new FormControl(null, [
          Validators.required
        ]));
        this.isEmployed = true;
      } else {
        this.step2AuthForm.removeControl('placeOfEmployment');
        this.isEmployed = false;
      }
    });
    this.statusData = [
      {
        value: 'employed',
        label: 'Employed'
      },
      {
        value: 'unemployed',
        label: 'Unemployed'
      },
      {
        value: 'student',
        label: 'Student'
      }
    ];
    this.incomeData = [
      {
        value: '1000',
        label: '1000$'
      },
      {
        value: '2000',
        label: '2000$'
      },
      {
        value: '3000',
        label: '3000$'
      },
      {
        value: '4000',
        label: '4000$'
      },
      {
        value: '5000',
        label: '5000$'
      }
    ];
  }
  public submit(e, form) {
    e.preventDefault();
    console.log(form.value);
    if (!form.invalid) {
      this.step2FormDataEmit.emit(form.value);
    } else { console.log('INVALID', form.value, form.controls); }
  }
}
