import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IMyDpOptions } from 'mydatepicker';

import { DatepickerValidation } from '../../../shared/validators/datepicker.validation';

@Component({
  selector: 'z-step1',
  styleUrls: [ './step1.component.scss' ],
  templateUrl: './step1.component.html',
})

export class Step1Component implements OnInit {
  public step1AuthForm: FormGroup;
  public datepickerOptions: IMyDpOptions;
  @Output() public step1FormDataEmit = new EventEmitter();
  constructor() {
    this.step1AuthForm = new FormGroup({
      name: new FormControl(null, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(15)
      ]),
      surname: new FormControl(null, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(20)
      ]),
      dateOfBirth: new FormControl(null, [
        Validators.required,
        DatepickerValidation.checkAge()
      ]),
      phone: new FormControl(null, [
        Validators.required
      ]),
      address: new FormControl(null, [
        Validators.required
      ]),
      city: new FormControl(null, [
        Validators.required
      ]),
      country: new FormControl(null)
    });
    this.datepickerOptions = {
      dateFormat: 'dd.mm.yyyy'
    };
  }
  public submit(e, form) {
    e.preventDefault();
    if (!form.invalid) {
      this.step1FormDataEmit.emit(form.value);
    }
  }

  public ngOnInit() {}
}
