import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'z-sign-up',
  styleUrls: [ './sign-up.component.scss' ],
  templateUrl: './sign-up.component.html',
})
export class SignUpComponent {
  public isVisibleFirstStep: boolean;
  public errorsLS;
  public userInfo;
  constructor(private auth: AuthService,
              private router: Router) {
    this.isVisibleFirstStep = true;
  }
  public getSecondStepData(e) {
    this.userInfo = Object.assign(this.userInfo, e);
    this.auth.signUp(this.userInfo).then((res) => {
      if (res === true) {
        this.router.navigate(['/login/signIn']);
      } else {
        this.errorsLS = res;
      }
    });
  }
  public getFirstStepData(e) {
    this.isVisibleFirstStep = false;
    this.userInfo = e;
  }
}
