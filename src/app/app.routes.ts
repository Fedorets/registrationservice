import { Routes } from '@angular/router';
import { HomeComponent } from './home';

import { DataResolver } from './app.resolver';
import { PageNotFoundComponent } from './404/404.component';
import { AuthGuard } from './guards/auth.guard';

export const ROUTES: Routes = [
  { path: '',
    component: HomeComponent,
    data: {status: 'Admin'},
    canActivate: [AuthGuard]
  },
  { path: 'home',
    component: HomeComponent,
    data: {status: 'Admin'},
    canActivate: [AuthGuard]
  },
  { path: 'login',
    loadChildren: './auth/auth.module#AuthModule',
    data: {status: 'Visitor'},
    canActivate: [AuthGuard]
  },
  { path: '**',    component: PageNotFoundComponent }
];
